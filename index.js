//Load express module with `require` directive
var express = require('express')
var index = express()

//Define request response in root URL (/)
index.get('/', function (req, res) {
    res.send('Hello World!!!')
})

//Launch listening server on port 8080
index.listen(8080, function () {
    console.log('App listening on port 8080!')
})
